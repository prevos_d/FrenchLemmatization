# gateplugin-Stanford_CoreNLP
GateNLP Plugin providing Stanford CoreNLP functionality

## License

The plugin source code is under the license added as LICENSE.txt

This plugin pulls in and uses the Stanford CoreNLP libraries. 
See http://stanfordnlp.github.io/CoreNLP/#license for more details.

## NOTE

This is a non-official version of the GATE plugin at the moment, 
the official version is still part of the GATE Subversion repository
here: https://sourceforge.net/p/gate/code/HEAD/tree/gate/trunk/
in plugins/Stanford_CoreNLP
