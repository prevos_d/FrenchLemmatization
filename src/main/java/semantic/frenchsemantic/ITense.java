package semantic.frenchsemantic;

public interface ITense {
	static final String PAST = "past";
	static final int PAST_INT = -1;
	static final String IMPERFECT = "imperfect";
	static final int IMPERFECT_INT = -2;
	static final String SIMPLEPAST = "simplePast";
	static final int SIMPLEPAST_INT = -2;
	
	static final String PRESENT = "present";
	static final int PRESENT_INT = 0;
	
	static final String FUTURE = "future";
	static final int FUTURE_INT = 2;
}
