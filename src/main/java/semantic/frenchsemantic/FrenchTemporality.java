package semantic.frenchsemantic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CorpusController;
import gate.Document;
import gate.Factory;
import gate.Gate;
import gate.util.GateException;
import gate.util.persistence.PersistenceManager;

public class FrenchTemporality {

	/** Variables needed */
	private HelsinkiAnalysis helsinkiAnalysis;
	private CorpusController annieControllerFR;
	private Corpus annieCorpusFR;
	private int result;

	public FrenchTemporality() throws IOException {
		helsinkiAnalysis = new HelsinkiAnalysis();
	}

	private void initAnnieFR() throws GateException, IOException {
		String gateHome = System.getenv().get("GATE_HOME");

		Properties props = System.getProperties();
		if (gateHome != null)
			props.setProperty("gate.home", gateHome);

		Gate.setGateHome(new File("GATE"));
		Gate.setPluginsHome(new File("GATE/plugins"));
		Gate.setSiteConfigFile(new File("GATE/gate.xml"));

		Gate.init();

		Logger.getLogger("com.jpetrak.gate").setLevel(Level.OFF);

		File filesave = new File(
				"./GATE/plugins/gateapplication-French/applications/french-ner.gapp");
		try {
			annieControllerFR = (CorpusController) PersistenceManager
					.loadObjectFromFile(filesave);
		}
		catch (FileNotFoundException e) {
			throw e;
		}
		annieCorpusFR = Factory.newCorpus("StandAloneAnnie corpus FR");
	}

	private List<Annotation> getAnnots(Document doc) {
		AnnotationSet defaultAnnotSet = doc.getAnnotations();
		// Create the annotation we will use
		Set<String> annotTypesRequired = new HashSet<String>();
		annotTypesRequired.add("Token");
		annotTypesRequired.add("Temporality");
		List<Annotation> allAnnots = gate.Utils
				.inDocumentOrder(defaultAnnotSet.get(annotTypesRequired));
		return allAnnots;
	}

	private String getTheVerbAnalysis(Collection<String> analyses) {
		System.out.println("Analyses : ");
		for (String analysis : analyses) {
			System.out.println(analysis);
			if (analysis.contains("+verb+")) {
				System.out.println("Choosen : " + analysis);
				return analysis;
			}
		}
		return null;
	}

	private void updateResultWithValue(int value) {
		switch (value) {
		case 0:
			if (result < 0)
				result += 1;
			else if (result > 0)
				result -= 1;
			break;
		default:
			result += value;
		} // switch
	}

	private void catchAndUpdate(String str) {
		int value = 0;
		if (ITense.PAST.equals(str))
			value = ITense.PAST_INT;
		else if (ITense.IMPERFECT.equals(str))
			value = ITense.IMPERFECT_INT;
		else if (ITense.SIMPLEPAST.equals(str))
			value = ITense.SIMPLEPAST_INT;
		else if (ITense.PRESENT.equals(str))
			value = ITense.PRESENT_INT;
		else if (ITense.FUTURE.equals(str))
			value = ITense.FUTURE_INT;
		else
			return;

		System.out.println("Total temporality of the word: " + value);
		updateResultWithValue(value);
	}

	private void updateResult(Document doc, Annotation annotation) {
		Collection<String> analyses = helsinkiAnalysis
				.getAnalyses(gate.Utils.stringFor(doc, annotation));
		if (analyses != null && !analyses.isEmpty()) {
			String analysis = getTheVerbAnalysis(analyses);
			if (analysis != null) {
				String[] split = analysis.split("\\+");
				for (String string : split) {
					catchAndUpdate(string);
				}
			}
		}
	}

	public int run(Document doc) throws Exception {
		this.result = 0;

		List<Annotation> annotations = getAnnots(doc);

		for (Annotation a : annotations) {
			String word = null;
			String type = null;

			if (gate.Utils.stringFor(doc, a) != null)
				word = gate.Utils.stringFor(doc, a);
			System.out.println(word);

			if (a.getFeatures().get("upos") != null) {
				type = a.getFeatures().get("upos").toString();
				type = type.replaceAll(" ", "");
			}
			System.out.println("Type : " + type);
			if (type != null && !type.isEmpty() && type.equals("VERB")) {
				updateResult(doc, a);
				System.out.println("Current temporality : " + result);
			} else if (a.getType().equals("Temporality"))
				updateResultWithValue(Integer
						.parseInt(a.getFeatures().get("value").toString()));
			System.out.println("======================");
		}
		return this.result;
	}

	public int run(String txt) throws Exception {
		initAnnieFR();
		Document doc = Factory.newDocument(txt);
		annieCorpusFR.add(doc);
		annieControllerFR.setCorpus(annieCorpusFR);
		annieControllerFR.execute();

		return run(doc);
	}

	public static void main(String[] args) throws Exception {
		String txt = String.join(" ", args);
		FrenchTemporality frenchTemporality = new FrenchTemporality();
		System.out.println(
				"Temporality of the text : " + frenchTemporality.run(txt));
	}
}
