package semantic.frenchsemantic;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;

import net.sf.hfst.NoTokenizationException;
import net.sf.hfst.Transducer;
import net.sf.hfst.TransducerAlphabet;
import net.sf.hfst.TransducerHeader;
import net.sf.hfst.UnweightedTransducer;
import net.sf.hfst.WeightedTransducer;

public class HelsinkiAnalysis {

	private Transducer transducer = null;

	public HelsinkiAnalysis() throws IOException {
		this("./fr.hfst.ol");
	}

	public HelsinkiAnalysis(String path) throws IOException {
		FileInputStream transducerfile = null;
		transducerfile = new FileInputStream(path);
		TransducerHeader h = new TransducerHeader(transducerfile);
		DataInputStream charstream = new DataInputStream(transducerfile);
		TransducerAlphabet a = new TransducerAlphabet(charstream,
				h.getSymbolCount());
		if (h.isWeighted()) {
			transducer = new WeightedTransducer(transducerfile, h, a);
		} else {
			transducer = new UnweightedTransducer(transducerfile, h, a);
		}
	}

	public Collection<String> getAnalyses(String word) {
		try {
			return transducer.analyze(word.toLowerCase());
		}
		catch (NoTokenizationException e) {
			return null;
		}
	}

}
