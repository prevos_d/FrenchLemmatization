package semantic.frenchsemantic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CorpusController;
import gate.Document;
import gate.Factory;
import gate.Gate;
import gate.util.GateException;
import gate.util.persistence.PersistenceManager;

public class FrenchLemmatizer {

	private CorpusController annieControllerFR;
	private Corpus annieCorpusFR;

	public FrenchLemmatizer() throws GateException, IOException {

	}// StandAloneAnnie

	private void initAnnieFR() throws GateException, IOException {
		String gateHome = System.getenv().get("GATE_HOME");

		Properties props = System.getProperties();
		if (gateHome != null)
			props.setProperty("gate.home", gateHome);

		Gate.setGateHome(new File("GATE"));
		Gate.setPluginsHome(new File("GATE/plugins"));
		Gate.setSiteConfigFile(new File("GATE/gate.xml"));

		Gate.init();

		Logger.getLogger("com.jpetrak.gate").setLevel(Level.OFF);

		File filesave = new File(
				"./GATE/plugins/gateapplication-French/applications/french-ner.gapp");
		try {
			annieControllerFR = (CorpusController) PersistenceManager
					.loadObjectFromFile(filesave);
		}
		catch (FileNotFoundException e) {
			throw e;
		}
		annieCorpusFR = Factory.newCorpus("StandAloneAnnie corpus FR");
	}

	private List<Annotation> getAnnots(Document doc) {
		AnnotationSet defaultAnnotSet = doc.getAnnotations();
		// Create the annotation we will use
		Set<String> annotTypesRequired = new HashSet<String>();
		annotTypesRequired.add("Token");
		List<Annotation> allAnnots = gate.Utils
				.inDocumentOrder(defaultAnnotSet.get(annotTypesRequired));
		return allAnnots;
	}

	private String getTheAnalysis(Collection<String> analyses, String word,
			String type) {

		System.err.println("Analysis of " + word + " - " + type);
		// If Helsinki does not have this word in its database
		if (analyses == null || analyses.isEmpty() || type == null
				|| type.isEmpty()) {
			System.err.println(
					"Choosen : " + word + " (Not found --> Word is returned)");
			return word;
		}

		word = word.replaceAll(" ", "");
		type = type.replaceAll(" ", "");
		String grammar = "NONE";
		String grammarCheck = "NONE";
		if ("NOUN".equalsIgnoreCase(type)) {
			grammar = "\\+commonNoun.*";
			grammarCheck = "+commonNoun";
		} else if ("VERB".equalsIgnoreCase(type)) {
			grammar = "\\+verb+.*";
			grammarCheck = "+verb+";
		} else if ("ADJ".equalsIgnoreCase(type)) {
			grammar = "\\+adjective.*";
			grammarCheck = "+adjective";
		} else if ("ADV".equalsIgnoreCase(type)) {
			grammar = "\\+adverb.*";
			grammarCheck = "+adverb";
		} else if ("PRON".equalsIgnoreCase(type)
				|| "CONJ".equalsIgnoreCase(type)
				|| "DET".equalsIgnoreCase(type)) {
			grammar = "\\+functionWord.*";
			grammarCheck = "+functionWord";
		}

		// If the POSType given by GATE is found in the Helsinky database
		// Return the effective word
		for (String analysis : analyses) {
			if (analysis.contains(grammarCheck)) {
				String lemma = analysis.replaceAll(grammar, "");
				if ((lemma.contains("+") && !lemma.contains("-"))
						&& (word.contains("-") && !word.contains("+"))) {
					lemma = lemma.replaceAll("\\+", "-");
				}
				if (lemma.contains("+") && !word.contains("+")) {
					lemma = lemma.replaceAll("\\+", "");
				}
				System.err.println(
						"Choosen : " + lemma.toLowerCase() + " (Found)");
				return lemma.toLowerCase();
			}
		}

		// If the POSType given by GATE is found in the Helsinky database
		// Return the first word of the analysis
		String lemma = analyses.toArray()[0].toString();
		lemma = lemma.substring(0, lemma.indexOf("+"));
		System.err.println(
				"Choosen : " + lemma + " (Not Found --> First analysis)");
		return lemma;
	}

	public String run(Document doc) throws Exception {
		HelsinkiAnalysis helsinkiAnalysis = new HelsinkiAnalysis();
		List<String> lemmatizedWords = new ArrayList<String>();

		List<Annotation> annotations = getAnnots(doc);

		for (Annotation a : annotations) {
			String word = null;
			String type = null;

			if (gate.Utils.stringFor(doc, a) != null)
				word = gate.Utils.stringFor(doc, a);
			System.err.println(word);

			if (a.getFeatures().get("upos") != null)
				type = a.getFeatures().get("upos").toString();
			System.err.println(type);
			Collection<String> analyses = helsinkiAnalysis
					.getAnalyses(gate.Utils.stringFor(doc, a));
			lemmatizedWords.add(getTheAnalysis(analyses, word, type));
			System.err.println("======================");
		}
		String lemmatizedTxt = "";

		for (String s : lemmatizedWords)
			lemmatizedTxt += s + " ";

		return lemmatizedTxt;
	}

	public String run(String txt) throws Exception {
		initAnnieFR();
		Document doc = Factory.newDocument(txt);
		annieCorpusFR.add(doc);
		annieControllerFR.setCorpus(annieCorpusFR);
		annieControllerFR.execute();

		return run(doc);
	}

	public static void main(String[] args) throws Exception {
		String txt = String.join(" ", args);
		if (txt.isEmpty()) {
			System.err.println("No args provided");
			return;
		}
		FrenchLemmatizer frenchLemmatizer = new FrenchLemmatizer();
		String result = frenchLemmatizer.run(txt);
		System.out.println("BEFORE");
		System.out.println(txt);
		System.out.println("======================");
		System.out.println("AFTER");
		System.out.println(result);
	}
}
