package semantic.frenchsemantic;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CorpusController;
import gate.Document;
import gate.Factory;
import gate.Gate;
import gate.util.persistence.PersistenceManager;

public class FrenchPolarity {

	private final static Logger logger = Logger.getLogger(FrenchPolarity.class);

	private CorpusController annieControllerFR;
	private Corpus annieCorpusFR;
	private Document doc;

	private List<Annotation> allAnnots;
	private AnnotationSet annotsForCalcul;
	private Set<String> annotTypesForPolarity;
	private Set<String> annotTypesForCalcul;

	private int result = 0;
	private int resultSentence = 0;
	private boolean isNeg = false;
	private float possibilityValue = 1;

	public FrenchPolarity(String txt) throws Exception {
		String gateHome = System.getenv().get("GATE_HOME");

		Properties props = System.getProperties();
		if (gateHome != null)
			props.setProperty("gate.home", gateHome);

		if (Gate.getGateHome() == null)
			Gate.setGateHome(new File("GATE"));
		if (Gate.getPluginsHome() == null)
			Gate.setPluginsHome(new File("GATE/plugins"));
		if (Gate.getSiteConfigFile() == null)
			Gate.setSiteConfigFile(new File("GATE/gate.xml"));

		Gate.init();

		Logger.getLogger("com.jpetrak.gate").setLevel(Level.OFF);

		File filesave = new File(
				"./GATE/plugins/gateapplication-French/applications/french-ner.gapp");
		try {
			annieControllerFR = (CorpusController) PersistenceManager
					.loadObjectFromFile(filesave);
		}
		catch (FileNotFoundException e) {
			throw e;
		}
		annieCorpusFR = Factory.newCorpus("StandAloneAnnie corpus FR");

		doc = Factory.newDocument(txt);
		annieCorpusFR.add(doc);
		annieControllerFR.setCorpus(annieCorpusFR);
		annieControllerFR.execute();
	}

	/**
	 * Print with the logger
	 * 
	 * @param level The log level
	 * @param o The object to print
	 */
	private void print(Level level, Object o) {
		logger.log(level, "\n" + o);
	}// print

	/**
	 * Print info about an annotation
	 * Display the type, the minorType, the string related to the annotation and
	 * the annotation's value
	 *
	 * @param ann The annotation you want to print info about
	 */
	private void printInfoAnnot(Annotation ann) {
		this.print(Level.DEBUG,
				ann.getType() + "+" + ann.getFeatures().get("minorType") + " - "
						+ gate.Utils.stringFor(doc, ann) + " ("
						+ ann.getFeatures().get("value") + ")");
	}// printInfoAnnot

	/**
	 * Display a sentence
	 * Print it like "Sentence : theSentence"
	 * Used when a new Sentence begin
	 *
	 * @param ann The sentence to print
	 */
	private void printNewSentence(Annotation ann) {
		this.print(Level.DEBUG, "");
		this.print(Level.DEBUG, "==============================");
		this.print(Level.DEBUG,
				ann.getType() + " : " + gate.Utils.stringFor(doc, ann));
		this.print(Level.DEBUG, "==============================");
	}// printNewSentence

	/**
	 * Print the result of the entire article
	 * Used at the end of {@link #calculate()}
	 */
	private void printEntireResult() {
		this.print(Level.DEBUG, "\n==============================");
		this.print(Level.DEBUG, "Result of the entire text : " + result);
		this.print(Level.DEBUG, "==============================\n");
	}

	/**
	 * Display a message when a break is found
	 * Used when a break is founded
	 * The break are defined under ./gazetteer/break.lst
	 */
	private void breakFound() {
		this.print(Level.DEBUG, "==============================\n");
		this.print(Level.DEBUG, "Next part of the sentence");
		this.print(Level.DEBUG, "==============================");
		resetPolarityVars();
	}

	/**
	 * Reset the variables of the class (used in {@link #calculate()})
	 * Used when a new sentence begin
	 * {@link #resultSentence}
	 * {@link #resultNeg}
	 * {@link #possibilityValue}
	 * {@link #negOn}
	 * {@link #negativeValue}
	 */
	private void resetPolarityVars() {
		resultSentence = 0;
		possibilityValue = 1;
		isNeg = false;
	}

	private void init() {
		AnnotationSet defaultAnnotSet = this.doc.getAnnotations();

		annotTypesForPolarity = new HashSet<String>();
		annotTypesForPolarity.add("Sentence");
		annotTypesForPolarity.add("Trends");
		annotTypesForPolarity.add("Qualif");
		annotTypesForPolarity.add("Break");

		annotTypesForCalcul = new HashSet<String>();
		annotTypesForCalcul.add("Trends");
		annotTypesForCalcul.add("Qualif");
		annotTypesForCalcul.add("Break");

		allAnnots = gate.Utils
				.inDocumentOrder(defaultAnnotSet.get(annotTypesForPolarity));
		annotsForCalcul = defaultAnnotSet.get(annotTypesForCalcul);
	}

	private float calculate() {
		for (Annotation searchForSentence : allAnnots) { // For all annotations
			// in the article
			if (searchForSentence.getType() == "Sentence") {
				this.printNewSentence(searchForSentence);
				AnnotationSet annotsOfTheSentence = annotsForCalcul
						.getContained(
								searchForSentence.getStartNode().getOffset(),
								searchForSentence.getEndNode().getOffset());
				List<Annotation> orderedAnnot = gate.Utils
						.inDocumentOrder(annotsOfTheSentence);

				resetPolarityVars();
				for (Annotation annot : orderedAnnot) {// For each annot
					printInfoAnnot(annot);
					if (annot.getFeatures().get("minorType").equals("break")) {
						// We admit that a new sentence begins
						result += (resultSentence * (isNeg == true ? -1 : 1))
								* possibilityValue;
						breakFound();
					} else {
						if (annot.getFeatures().get("minorType")
								.equals("possibility"))
							possibilityValue *= Float.parseFloat(
									(String) annot.getFeatures().get("value"));
						else if (annot.getFeatures().get("minorType")
								.equals("negation"))
							isNeg = true;
						else {
							resultSentence += Integer.parseInt(
									(String) annot.getFeatures().get("value"));
						}
					} // else
				} // For each annot
				result += (resultSentence * (isNeg == true ? -1 : 1))
						* possibilityValue;
			} // if sentence is found
		} // For all annots
		printEntireResult();
		return result;
	}

	public float run() throws Exception {
		init();

		return calculate();
	}

	public static void main(String[] args) throws Exception {
		String txt = String.join(" ", args);
		if (txt.isEmpty()) {
			System.err.println("No args provided");
			return;
		}

		FrenchLemmatizer frenchLemmatizer = new FrenchLemmatizer();
		String result = frenchLemmatizer.run(txt);

		FrenchPolarity frenchPolarity = new FrenchPolarity(result);
		float polarity = frenchPolarity.run();
		System.out.println("Result : " + polarity);
	}
}
