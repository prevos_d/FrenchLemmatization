# FrenchSemantic Documentation

## 1. Requirements 
### 1.1. Libraries
- Hfst-ol.jar [https://sourceforge.net/projects/hfst/files/](https://sourceforge.net/projects/hfst/files/)
- JRE/JDK 7/8
- Maven

## 2. How to.
### 2.1. Temporality part
#### Edit the existing list
1. Go under `FrenchSemantic/GATE/PLUGINS/gateapplication-French/resources/gazetteer`
2. Edit the file named `temporality.lst`

#### Create a new list and add it to the detect process
1. Go under `FrenchSemantic/GATE/PLUGINS/gateapplication-French/resources/gazetteer`
2. Create a file like *name_of_the_file*.lst
3. Edit the file named `lists3.def`
4. Add a new entry on a new line following this model : *name_of_the_file*.lst:temporality:temporality:french:Temporality

#### Add a new entry to a list
1. Go under `FrenchSemantic/GATE/PLUGINS/gateapplication-French/resources/gazetteer`
2. Open the *lst* file you wan to edit
3. Add a new entry on a new line following this model : *word_to_detect*:value=*value_attributed_to_the_word*[^1]

[^1]: Past is negative. Present is null. Future is positive. You can refer to the file FrenchSemantic/GATE/PLUGINS/gateapplication-French/resources/gazetteer/temporality.lst